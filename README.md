Barcode39 Font
==============

When you want to print `1234` in a barcode surround it with `*` characters
from each side, like `*1234*`. Assign the font to the text and
the scanner will read it. Enjoy!
